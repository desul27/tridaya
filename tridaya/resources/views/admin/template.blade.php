@include('admin.header')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        {{-- <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
        </div> --}}

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                {{-- <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li> --}}
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Navbar Search -->
                <li class="nav-item">
                    {{-- <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a> --}}
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            {{-- <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div> --}}
                        </form>
                    </div>
                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{url('/')}}" class="brand-link">
                <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}" alt="Tridaya Company" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">TRIDAYA COMPANY</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('adminlte/dist/img/sultanikal.jpg')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                      <a href="#" class="d-block">Bos Ikal</a>
                        {{-- <a href="#" class="d-block">{{ Auth::user()->name }}</a> --}}
                    </div>
                </div>

                <!-- SidebarSearch Form -->
                {{-- <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-sidebar">
                                <i class="fas fa-search fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div> --}}

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item menu-open">
                            <a href="/" class="nav-link  {{Request::is('/')?'active':''}}">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Laporan Penjualan</p>
                            </a>
                        </li>

                        {{-- Transakasi --}}
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-envelope-open-text"></i>
                                <p>
                                    Transakasi
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/transaksijual" class="nav-link {{Request::is('transaksijual')?'active':''}}">
                                        <i class="fas fa-shipping-fast nav-icon"></i>
                                        <p>Transakasi Jual</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/transaksibeli" class="nav-link {{Request::is('transaksibeli')?'active':''}} ">
                                        <i class="fas fa-people-carry nav-icon"></i>
                                        <p>Transaksi Beli</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        {{-- PRODUK --}}
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-server"></i>
                                <p>
                                    Barang
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/product" class="nav-link {{Request::is('product')?'active':''}}">
                                        <i class=" fas fa-clipboard-list nav-icon"></i>
                                        <p>List Barang</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="/product/insert" class="nav-link {{Request::is('product/insert')?'active':''}}">
                                        <i class="fas fa-plus  nav-icon"></i>
                                        <p>Tambah Barang</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        {{-- DATA CUSTOMERS --}}
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Customer
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="/customer" class="nav-link {{Request::is('customer')?'active':''}}">
                                        <i class="far fa-address-card nav-icon "></i>
                                        <p>List Customers</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                  <a href="/customer/piutang" class="nav-link {{Request::is('customer/piutang')?'active':''}}">
                                    <i class=" fas fa-money-check nav-icon"></i>
                                    <p>Customer Piutang</p>
                                  </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/customer/insert" class="nav-link {{Request::is('customer/insert')?'active':''}}">
                                        <i class="fas fa-plus nav-icon"></i>
                                        <p>Add Customer</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        {{-- DATA SUPLIER --}}
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-user-friends"></i>
                                <p>
                                    Suplier
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/suplier" class="nav-link {{Request::is('suplier')?'active':''}}">
                                        <i class="far fa-address-book nav-icon"></i>
                                        <p>List Supliers</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/suplier/hutang" class="nav-link {{Request::is('suplier/hutang')?'active':''}}">
                                        <i class="fas fa-money-check-alt nav-icon"></i>
                                        <p>Suplier Hutang </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/suplier/insert" class="nav-link {{Request::is('suplier/insert')?'active':''}} ">
                                        <i class="fas fa-plus nav-icon"></i>
                                        <p>Add Suplier</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-desktop"></i>
                                <p>
                                    Admin
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/user" class="nav-link {{Request::is('user')?'active':''}}">
                                        <i class="fas fa-grip-lines nav-icon"></i>
                                        <p>List Admin</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="/user/insert" class="nav-link {{Request::is('user/insert')?'active':''}} ">
                                        <i class="fas fa-user-plus nav-icon"></i>
                                        <p>Add Admin</p>
                                    </a>
                                </li>
                            </ul>
                        </li>


                        {{--LOGOUT FORM  --}}
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                            </a>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li> --}}
                        <li class="nav-item">
                            <a class="nav-link" href="#" >
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                            </a>
                             <form id="logout-form" action="#" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        {{--  --}}
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
            {{--  --}}



        </div>
        <!-- /.content-wrapper -->
@include('admin.footer')
