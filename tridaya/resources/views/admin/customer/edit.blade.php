@extends('admin/template')
@section('title')
Edit Product
@endsection('title')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Product</h3>
                </div>

            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script>
function previewImage(){
  const image = document.querySelector('#image');
     const imgPreview = document.querySelector('.img-preview')
     imgPreview.style.display = 'block';
     const oFReader = new FileReader();
     oFReader.readAsDataURL(image.files[0]);
     oFReader.onload = function(oFREvent) {
     imgPreview.src = oFREvent.target.result;
     }
}

</script>
@endsection('content')
