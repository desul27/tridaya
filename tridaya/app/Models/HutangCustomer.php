<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HutangCustomer extends Model
{
    use HasFactory;
    protected $table = 'hutang_customer';

   protected $fillable = [
       'id_customer',
       'total',
   ];

   public function customer()
   {
       return $this->belongsTo('App\Models\Customer');
   }

}
