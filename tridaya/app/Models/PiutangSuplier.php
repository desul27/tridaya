<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PiutangSuplier extends Model
{
    use HasFactory;


    protected $table = 'piutang_suplier';

    protected $fillable = [
        'id_suplier',
        'total',
    ];

    public function suplier()
    {
        return $this->belongsTo('App\Models\Suplier');
    }
}
