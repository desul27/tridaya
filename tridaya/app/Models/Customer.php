<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    <?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';

    protected $fillable = [
        'nama_customer',
        'alamat',
        'telepon',

    ];

    public function penjualan()
    {
        return $this->hasMany('App\Models\Penjualan');
    }

    public function hutang()
    {
        return $this->hasOne('App\Models\HutangCustomer');
    }
}

}
