<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suplier extends Model
{
    use HasFactory;
    protected $table = 'supplier';

    protected $fillable = [
        'nama_suplier',
        'alamat',
        'telepon',
        'email',
    ];

    public function barang()
    {
        return $this->belongsToMany('App\Models\Barang', 'barang_supplier');
    }
}
