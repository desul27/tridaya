<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
class Product extends Model{
    protected $table = 'barang';
    protected $fillable = [
        'nama',
        'harga',
        'satuan',
        'stock',
        'deskripsi',
        'image',
    ];

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori');
    }

    public function suplier()
    {
        return $this->belongsToMany('App\Models\Suplier', 'barang_supplier');
    }
}
