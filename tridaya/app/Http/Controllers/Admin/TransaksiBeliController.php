<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksibeli;
class TransaksiBeliController extends Controller
{

    public function index()
    {
        return view('admin/transaksi-beli/list');
    }
    public function insert()
    {
        return view('admin/transaksi-beli/insert');
    }

}
