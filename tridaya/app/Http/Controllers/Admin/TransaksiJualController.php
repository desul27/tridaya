<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksijual;
class TransaksiJualController extends Controller
{

    public function index()
    {
          return view('admin/transaksi-jual/list');
    }

    public function insert()
    {
          return view('admin/transaksi-jual/insert');
    }

}
