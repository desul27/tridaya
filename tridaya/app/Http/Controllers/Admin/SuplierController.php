<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Suplier;
class SuplierController extends Controller
{

    public function index()
    {
        return view('admin/suplier/list');
    }

    public function insert()
    {
        return view('admin/suplier/insert');
    }
}
